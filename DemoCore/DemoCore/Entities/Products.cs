﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DemoCore.Entities
{
    public partial class Products
    {
        [Key]
        public int ProductID { get; set; }
        [Required]
        [StringLength(50)]
        public string ProductCode { get; set; }
        [Required]
        [StringLength(250)]
        public string ProductName { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public decimal Amount { get; set; }
        [Required]
        [StringLength(250)]
        public string Description { get; set; }
        public int StatusID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateCreated { get; set; }
    }
}
