﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoCore.Business;
using DemoCore.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductBusiness ProductBusiness;
        public ProductController(ProductBusiness productBusiness)
        {
            ProductBusiness = productBusiness;
        }

        [HttpGet("SearchList")]
        public List<Products> SearchListProduct()
        {

            return ProductBusiness.SearchList();
        }

        [HttpGet("GetByID/{productID}")]
        public Products GetProductByID(int productID)
        {

            return ProductBusiness.GetByID(productID);
        }

        [HttpPost("Create")]
        public Products CreateProduct([FromBody] Products bodyData)
        {
            return ProductBusiness.Create(bodyData);
        }
    }
}
