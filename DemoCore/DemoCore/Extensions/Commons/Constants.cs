﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoCore.Extensions.Commons
{
    public enum StatusID
    {
        Deleted = -1,
        Active = 1,
    }
}
