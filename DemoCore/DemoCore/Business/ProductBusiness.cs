﻿using DemoCore.DBContext;
using DemoCore.Entities;
using DemoCore.Extensions.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoCore.Business
{
    public class ProductBusiness
    {
        private CoreContext CoreContext;

        public ProductBusiness(CoreContext coreContext)
        {
            CoreContext = coreContext;
        }

        public List<Products> SearchList()
        {
            var lstProducts = CoreContext.Products
                .AsEnumerable()
                .Where(p => p.StatusID != (int)StatusID.Deleted)
                .ToList();

            return lstProducts;
        }

        public Products GetByID(int productID)
        {
            return CoreContext.Products
                .AsEnumerable()
                .Where(p => p.ProductID == productID)
                .FirstOrDefault();
        }

        public Products Create(Products dataCreate)
        {
            var product = new Products()
            {
                ProductName = dataCreate.ProductName,
                ProductCode = dataCreate.ProductCode,
                Amount = dataCreate.Amount,
                Description = dataCreate.Description,
                DateCreated = DateTime.Now,
                StatusID = (int)StatusID.Active,
            };

            CoreContext.Products.Add(product);
            CoreContext.SaveChanges();

            return product;
        }
    }
}
